<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'infp') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <!-- <link href="resources/css/app.css" rel="stylesheet"> -->

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
   <div class="container-fluid pt-3">
   <div class="row">
                <div class="col">
                   <div class="row">
                        <div class="col-3 ">
                        <a class="navbar-brand" href="https://www.infpguinnee.com">
                    <!-- {{ config('app.name', 'infp') }} -->
                    <img src="{{asset('images/ministre.png')}}" alt="" with="100px" height="100px">
                   
                </a>
                        </div>
                        <div class="ps-0 pt-3 col-5 text-center">
                            <span style="font-weight:'bold';">RÉPUBLIQUE DE GUINÉE <br></span>
                            <span style="font-size:8px; color:grey">Travail-Justice-Solidarité <br></span>
                            <span style="font-size:10px;">MINISTÉRE DU TRAVAIL ET DE LA <br></span>
                            <span style="font-size:10px;">FONCTION PUBLIQUE <br></span>
                        </div>
                   </div>
                       
               
                       
                       
                    </div>
                    <div class="col text-center pe-4">
                    <a class=" " href="https://www.infpguinnee.com">
                    <!-- {{ config('app.name', 'infp') }} -->
                    <img src="{{asset('images/branding.png')}}" alt="" with="100px" height="100px">
                   
                        </a>

                    </div>
                   
                    <div class="col">
                        <div class="row ">
                            <div class="col">

                            </div>
                            <div class="col-4">
                            <a class="" href="https://www.infpguinnee.com">
                    <!-- {{ config('app.name', 'infp') }} -->
                    <img src="{{asset('images/logo.png')}}" alt="" with="100px" height="100px">
                   
                </a>
                            </div>
                        </div>

                    </div>
            </div>
   </div>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

            <div class="container">
              

        
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                                    <a class="nav-link active " href="https://www.infpguinee.com">Accueil</a>
                                </li>
                    </ul>
                   

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                   <li class="nav-item">
                   
                   
                
                   </li>
                        <!-- Authentication Links -->
                        <!-- @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest -->
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
