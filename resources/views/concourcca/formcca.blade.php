@extends('layouts.app')
@section('content')

    <div class="container">
    <h1>Inscription Concour CCA</h1>

        
    <form action="/inscriptioncca" method="post" enctype="multipart/form-data">
    @csrf
        @if($message = Session::get('success'))

            <div class="alert alert-success">
                <strong>{{$message}}  |  <a class="nav-link" href="https://www.infpguinee.com">Retourner a la page d'Accueil d'infp</a></strong>
            </div>

        @endif

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>

        @endif

        <div class="row">
        <div class="m-3 col">
            <label for="nom">Nom:</label>
            <input type="text" id="nom" class="form-control" placeholder="Saisir votre nom ..." name="nom"/>
        </div>
        <div class="m-3 col">
            <label for="prenom">Prenom:</label>
            <input type="text" id="prenom" class="form-control" placeholder="Saisir votre prenom ..." name="prenom"/>
        </div>
        <div class="m-3 col">
            <label for="matricule">Matricule:</label>
            <input type="text" id="matricule" class="form-control" placeholder="Saisir votre matricule ..." name="matricule"/>
        </div>
        </div>
       
       <div class="row">
       <div class="m-3 col">
            <label for="email">Email:</label>
            <input type="email" id="email" class="form-control" placeholder="Saisir votre email ..." name="email"/>
        </div>
        <div class="m-3 col">
            <label for="tel">Tel:</label>
            <input type="text" id="tel" class="form-control" placeholder="Saisir votre numero de telephone ..." name="tel"/>
        </div>

        <div class="col m-3">
        <label class="custom-file-label" for="choisirb">Dernier Bulletin:</label>
            <input type="file" id="choisirb" class="custom-file-input" name="file_dernierbultin"/>
           
        </div>
       </div>

       <div class="row">
       <div class="col m-3">
        <label class="custom-file-label" for="photo">Photo :</label>
            <input type="file" id="photo" class="custom-file-input" name="file_photo"/>
        </div>
        <div class="col m-3">
        <label class="custom-file-label" for="pieceidentite">Pièce d'Identité:</label>
            <input type="file" id="pieceidentite" class="custom-file-input" name="file_pieceidentite"/>
        </div>
        <div class="col m-3">
        <label class="custom-file-label" for="lettremotivation">Lettre Motivation:</label>
            <input type="file" id="lettremotivation" class="custom-file-input" name="file_lettremotivation"/>
        </div>
       </div>

       <div class="row">
       <div class="col m-3">
        <label class="custom-file-label" for="lettrerecomendation">Lettre Recomendation:</label>
            <input type="file" id="lettrerecomendation" class="custom-file-input" name="file_lettrerecomendation"/>
        </div>
        <div class="col m-3">
        <label class="custom-file-label" for="diplome">Diplôme:</label>
            <input type="file" id="diplome" class="custom-file-input" name="file_diplome"/>
        </div>
        <div class="col m-3">
        <label class="custom-file-label" for="arreteengagement">Arrété d'Engagement:</label>
            <input type="file" id="arreteengagement" class="custom-file-input" name="file_arreteengagement"/>
        </div>
       </div>


      <div class="text-center ">
            <button class="btn btn-success px-4" type="submit" name="submit">Envoyer</button>
      </div>

    </form>
    </div>
@endsection