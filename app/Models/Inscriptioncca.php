<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscriptioncca extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'prenom',
        'matricule',
        'email',
        'tel',
        'file_dernierbultin',
        'file_photo',
        'file_pieceidentite',
        'file_lettremotivation',
        'file_lettrerecomendation',
        'file_diplome',
        'file_arreteengagement',
    ];
}
