<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscriptionsst extends Model
{
    protected $fillable = [
        'nom',
        'prenom',
        'email',
        'tel',
        'file_cv',
        'file_actenais',
        'file_photo',
        'file_pieceidentite',
        'file_lettremotivation',
        'file_lettrerecomendation',
        'file_diplome',
        'file_arreteengagement',
    ];
    use HasFactory;
}
