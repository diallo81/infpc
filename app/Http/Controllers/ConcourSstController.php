<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inscriptionsst;
class ConcourSstController extends Controller
{
    //
    public function index(){

        return view('concoursst.concoursst');
    }

    public function formSend(Request $req){
        $req->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'email'=>'required',
            'tel'=>'required',
            'file_cv'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_actenais'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_photo'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_pieceidentite'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_lettremotivation'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_lettrerecomendation'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_diplome'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_arreteengagement'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',

        ]);

     
        if($req->hasfile('file_cv','file_actenais','file_photo','file_pieceidentite',
        'file_pieceidentite', 'file_lettremotivation','file_lettrerecomendation',
        'file_diplome','file_arreteengagement'
        )){
            $file_cv = $req->file('file_cv')->store(options:'fichiers');
            $file_actenais = $req->file('file_actenais')->store(options:'fichiers');
            $file_photo = $req->file('file_photo')->store(options:'fichiers');
            $file_pieceidentite = $req->file('file_pieceidentite')->store(options:'fichiers');
            $file_lettremotivation = $req->file('file_lettremotivation')->store(options:'fichiers');
            $file_lettrerecomendation = $req->file('file_lettrerecomendation')->store(options:'fichiers');
            $file_diplome = $req->file('file_diplome')->store(options:'fichiers');
            $file_arreteengagement = $req->file('file_arreteengagement')->store(options:'fichiers');
        }

        $inscriptionCca = Inscriptionsst::create([
            'nom'=> $req->nom,
            'prenom'=>$req->prenom,
            'email'=>$req->email,
            'tel'=>$req->tel,
            'file_cv'=> $file_cv?? null,
            'file_actenais'=> $file_actenais?? null,
            'file_photo'=>$file_photo ?? null,
            'file_pieceidentite'=>$file_pieceidentite ?? null,
            'file_lettremotivation'=>$file_lettremotivation ?? null,
            'file_lettrerecomendation'=>$file_lettrerecomendation ?? null,
            'file_diplome'=>$file_diplome ?? null,
            'file_arreteengagement'=>$file_arreteengagement ?? null,
        ]);

        return redirect()->back()->with('success','Inscription SST effectuer avec succées !');
    }
}
