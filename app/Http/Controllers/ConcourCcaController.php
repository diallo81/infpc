<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFil;
use App\Models\Inscriptioncca;

class ConcourCcaController extends Controller
{
    public function index(){

        return view('concourcca.formcca');
    }

    public function formSend(Request $req){
        $req->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'matricule'=>'required',
            'email'=>'required',
            'tel'=>'required',
            'file_dernierbultin'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_photo'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_pieceidentite'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_lettremotivation'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_lettrerecomendation'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_diplome'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',
            'file_arreteengagement'=>'required|mimes:csv,txt,xlx,xls,pdf,jpeg,jpg|max:2048',

        ]);

     
        if($req->hasfile('file_dernierbultin','file_photo','file_pieceidentite',
        'file_pieceidentite', 'file_lettremotivation','file_lettrerecomendation',
        'file_diplome','file_arreteengagement'
        )){
            $file_dernierbultin = $req->file('file_dernierbultin')->store(options:'fichiers');
            $file_photo = $req->file('file_photo')->store(options:'fichiers');
            $file_pieceidentite = $req->file('file_pieceidentite')->store(options:'fichiers');
            $file_lettremotivation = $req->file('file_lettremotivation')->store(options:'fichiers');
            $file_lettrerecomendation = $req->file('file_lettrerecomendation')->store(options:'fichiers');
            $file_diplome = $req->file('file_diplome')->store(options:'fichiers');
            $file_arreteengagement = $req->file('file_arreteengagement')->store(options:'fichiers');
        }

        $inscriptionCca = Inscriptioncca::create([
            'nom'=> $req->nom,
            'prenom'=>$req->prenom,
            'matricule'=>$req->matricule,
            'email'=>$req->email,
            'tel'=>$req->tel,
            'file_dernierbultin'=> $file_dernierbultin ?? null,
            'file_photo'=>$file_photo ?? null,
            'file_pieceidentite'=>$file_pieceidentite ?? null,
            'file_lettremotivation'=>$file_lettremotivation ?? null,
            'file_lettrerecomendation'=>$file_lettrerecomendation ?? null,
            'file_diplome'=>$file_diplome ?? null,
            'file_arreteengagement'=>$file_arreteengagement ?? null,
        ]);

        return redirect()->back()->with('success','Inscription CCA effectuer avec succées !');
    }

}


