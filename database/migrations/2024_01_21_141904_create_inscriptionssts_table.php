<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inscriptionssts', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->string('tel')->nullable();
            $table->string('file_cv');
            $table->string('file_actenais');
            $table->string('file_photo');
            $table->string('file_pieceidentite');
            $table->string('file_lettremotivation');
            $table->string('file_lettrerecomendation');
            $table->string('file_diplome');
            $table->string('file_arreteengagement');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inscriptionssts');
    }
};
