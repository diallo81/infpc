<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/inscriptioncca', [App\Http\Controllers\ConcourCcaController::class, 'index'])->name('concourcca.formcca');
Route::post('/inscriptioncca', [App\Http\Controllers\ConcourCcaController::class, 'formSend'])->name('concourcca.formcca');
Route::get('/inscriptionsst', [App\Http\Controllers\ConcourSstController::class, 'index'])->name('concoursst.concoursst');
Route::post('/inscriptionsst', [App\Http\Controllers\ConcourSstController::class, 'formSend'])->name('concoursst.concoursst');
